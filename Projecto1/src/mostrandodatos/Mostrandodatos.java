package mostrandodatos;

import java.util.Scanner;

public class Mostrandodatos {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Cual es el nombre");		
		String name=in.nextLine();
		System.out.println("Cual es el apellido");
		String surname=in.nextLine();
		
		System.out.println("El nombre de la persona es " + name + " y el apellido es " + surname);
		in.close(); 
	}

}
